# Overview
Q-Graph is a graph processing system that can process multiple queries concurrently in an optimized way.

# Architecture
Several components, Master, Worker, Input, Output, Plotting, Apps

![Architecture](Architecture_scaled.png "System Architecture")

# Shortest Path Example

## Local Test Runner
In order to execute a series of shortest path queries on your machine, you have to perform the following steps.

0) Get the Project and set up an eclipse project using Maven.

1) The class SPLocalTestClusterMain in the Java project provides basic functionality to run QGraph on your machine.

2) Define the arguments for SPLocalTestClusterMain, e.g.
```
configs/configuration.properties configs/clusterconfig_local2.txt input_graphs\bw_red.bin testplans/bw1.txt
```
The first argument is the configuration file to be used. Have a look at the parameters to ensure proper execution. The second argument specifies the clusterconfig file, i.e., the ports and hostnames of your (local) test cluster. You can use any of the provided files for first tests. The third argument defines the graph file -- just take the specified one to test functionality.
The fourth argument is the testplan, i.e., the shortest path queries to be executed. We have already provided a few testplans on different graphs that you can use.

3) Run the class and wait for termination. You will find the respective output of each of the 512 queries in the output folder of the project. Moreover, find a lot of plots of different system parameters and performance metrics (such as query latency) in the folder output/stats/plots1.

### More about Local Tests

SPLocalTestClusterMain can be used to start a local test cluster with master and workers on the local machine.
In the java project the local test runner can be started using the main method of mthesis.concurrent_graph.apps.shortestpath.SPSingleMasterMain

```
Usage: [configFile] [clusterConfigFile] [inputFile] [optional extraJvmPerWorker-bool]
```

configFile is a java properties file that defines the system configurations such as log level, networking configuration and operation modes. A default configuration file can be found in configs/configuration.properties

clusterConfigFile configures the local test cluster. For example configs/clusterconfig_local8.txt configures a local test with 8 workers.

inputFile must be a graph file TODO Format. One possibility to create this file is using https://github.com/jgrunert/SimpleOSM2Graph by converting OSM data to road network graphs.

If extraJvmPerWorker [experimental] is enabled, each worker will be started in a sepatarate jvm.

## Cluster Deployment
Q-Graph was tested on different deployments. It supports running on distributed machines

## Input Partitioning
There are different input partitioning strategies available, for example: Direct, Hashed or Hotspot-Clustering. 
Direct means that the partitions are directly assigned, the vertex order is taken from the input files. 
Hashing means that vertices are distributed on partitions using a hash function.

The PartitioningStrategySelector selects a partitioning strategy based on the InputPartitioner config value.
